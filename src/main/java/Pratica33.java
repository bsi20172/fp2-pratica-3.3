import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        
        Matriz matriz2 = new Matriz(3, 2);
        double[][] m2 = matriz2.getMatriz();
        m2[0][0] = 1.0;
        m2[0][1] = 1.1;
        m2[1][0] = 2.0;
        m2[1][1] = 2.1;
        m2[2][0] = 3.0;
        m2[2][1] = 3.1;
        
        Matriz matriz3 = new Matriz(2, 3);
        double[][] m3 = matriz3.getMatriz();
        m3[0][0] = 0.0;
        m3[0][1] = 0.1;
        m3[0][2] = 0.2;
        m3[1][0] = 1.0;
        m3[1][1] = 1.1;
        m3[1][2] = 1.2;
        
        Matriz prod = orig.prod(matriz3);
        Matriz soma = orig.soma(matriz2);
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz soma: " + soma);
        System.out.println("Matriz produto: " + prod);
    }
}
